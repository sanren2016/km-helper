<?php

use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::prefix('/project')->group(function (){
    Route::get('/list',[ProjectController::class,'list']);
    Route::get('/listPage',[ProjectController::class,'listPage']);
    Route::get('/addPage',[ProjectController::class,'addPage']);
});


