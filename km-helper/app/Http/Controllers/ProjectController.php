<?php


namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class ProjectController extends BaseController
{

    public function list(): JsonResponse
    {
        Log::info('project list');
        $data = [
            'code' => 200,
            'success' => true,
            'msg' => 'success',
            'body' => 'project list',
            'extra' => null
        ];
        return response()
            ->json($data);
    }

    public function listPage(): Response
    {
        return response()
            ->view('project/list');
    }

    public function addPage(){
        return view('project/add');
    }
}
